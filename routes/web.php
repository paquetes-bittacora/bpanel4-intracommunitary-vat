<?php

declare(strict_types=1);

use Bittacora\Bpanel4\IntracomunitaryVat\Http\Controllers\IntracomunitaryVatPublicController;
use Illuminate\Support\Facades\Route;

Route::prefix('/iva-intracomunitario')->name('bpanel4-intracomunitary-vat.')->middleware(['web'])->group(function () {
    Route::post('/enviar-numero', [IntracomunitaryVatPublicController::class, 'apply'])->name('apply');
    Route::get('/quitar-numero', [IntracomunitaryVatPublicController::class, 'remove'])->name('remove');
});
