<?php

namespace Bittacora\Bpanel4\IntracomunitaryVat\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Bpanel4\Clients\Contracts\ClientService;
use Bittacora\Bpanel4\IntracomunitaryVat\Services\IntracomunitaryVatService;
use Ibericode\Vat\Vies\ViesException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

class IntracomunitaryVatPublicController extends Controller
{
    public function __construct(
        private readonly IntracomunitaryVatService $intracomunitaryVatService,
        private readonly Redirector $redirector,
        private readonly ClientService $clientService,
    ) {
    }

    public function apply(Request $request): RedirectResponse
    {
        $vatNumber = $request->get('vat_number');

        if(!$this->intracomunitaryVatService->isUserInEurope()) {
            return $this->redirector->back()->with([
                'alert-danger' => __('Debe estar en la UE para aplicar el nº de IVA')
            ]);
        }

        try {
            if (!$this->intracomunitaryVatService->isVatNumberValid($vatNumber ?? '')) {
                return $this->redirector->back()->with([
                    'alert-danger' => __('El Nº de IVA no es válido o no está registrado')
                ]);
            }
        } catch (ViesException $e) {
            report($e);
            return $this->redirector->back()->with([
                'alert-danger' => __('No se pudo verificar el Nº de IVA por un error externo')
            ]);
        }

        $cart = $this->clientService->getClientCart();
        $cart->setVatNumber($vatNumber);
        $cart->setCountryCode($this->intracomunitaryVatService->getCountryCodeForCurrentIp());
        $cart->save();

        return $this->redirector->back()->with([
            'alert-success' => __('El Nº de IVA se guardó correctamente')
        ]);
    }

    public function remove(): RedirectResponse
    {
        $cart = $this->clientService->getClientCart();

        $cart->setVatNumber(null);
        $cart->setCountryCode(null);
        $cart->save();

        return $this->redirector->back()->with([
            'alert-success' => __('El Nº de IVA se quitó correctamente')
        ]);
    }
}
