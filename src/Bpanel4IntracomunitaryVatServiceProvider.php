<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\IntracomunitaryVat;

use Illuminate\Support\ServiceProvider;

class Bpanel4IntracomunitaryVatServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
    }
}
