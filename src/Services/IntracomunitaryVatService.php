<?php

namespace Bittacora\Bpanel4\IntracomunitaryVat\Services;

use Ibericode\Vat\Countries;
use Ibericode\Vat\Geolocator;
use Ibericode\Vat\Validator;
use Ibericode\Vat\Vies\ViesException;
use Illuminate\Http\Request;

class IntracomunitaryVatService
{
    public function __construct(
        private readonly Geolocator $geolocator,
        private readonly Countries $countries,
        private readonly Validator $validator,
        private readonly Request $request,
    ) {
    }

    public function getCountryCodeForCurrentIp(): string
    {
        return $this->geolocator->locateIpAddress($this->request->ip()) ?? '??';
    }

    public function isUserInEurope(): bool
    {
        return $this->countries->isCountryCodeInEU($this->getCountryCodeForCurrentIp());
    }

    /**
     * @throws ViesException
     */
    public function isVatNumberValid(string $vatNumber): bool
    {
        return $this->validator->validateVatNumber($vatNumber);
    }
}
